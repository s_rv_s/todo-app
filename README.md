# TodoApp
Sample ToDo App using angular, express, mongo stack
## Development server
### Backend server:
run in console `cd server` then run `npm start` for a start api server. It's allowed by url `http://localhost:8080/`
### Frontend App:
run in console `cd todo-app` then run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Requires
App required `mongodb` server and database named `todoApp` on 27027 port at localhost.
On first start API server fill database with initial user

## Credentials. 
username: `admin`, password: `pass`