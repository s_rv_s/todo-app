const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const UserSchema = mongoose.Schema({
    username: {
        type: String,
        index: true
    },
    password: { type: String },
    firstName: { type: String },
    lastName: { type: String }
}).set("toJSON", {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {   delete ret._id; delete ret.password }
});

const salt = bcrypt.genSaltSync(10);

function cryptPassword(password) {
    return bcrypt.hashSync(password, salt)
}
const UserModel = mongoose.model('User', UserSchema);

/*todo remove for production
   it's create user only for local test */
    const user = {
        username: "admin",
        password: cryptPassword("pass"),
        firstName: 'admin',
        lastName: 'lad',
    };

    UserModel.findOne({
        username: 'admin'
    }, function (err, u) {
        if(!u) {
            const nUser = new UserModel(user);
            nUser.save(function (err) {
                if(err)
                    console.error("!!! cant create test admin user!!!", err);
            })
        }
    });
/*--*/

module.exports.findByUsername = function (username, callback){
    UserModel.findOne({
        username: username
    },callback)
};
module.exports.findById = function (id, callback){
    UserModel.findOne({
        _id: id
    },callback)
};
module.exports.verifyPassword = function (password, user) {
    return bcrypt.compareSync(password, user.password);
};
