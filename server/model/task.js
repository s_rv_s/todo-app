const mongoose = require('mongoose');

const TaskSchema = mongoose.Schema({
    name: { type: String },
    description: { type: String },
    status: { type: Number },
    orderPosition: { type: Number },
    creatorId: { type: String, index: true},
    parentTaskId: {type: String },
    createdAt: { type: String },
    modifiedAt: { type: String },
    expiredAt: { type: String },
    isDeleted: { type: Boolean, default: false},
}).set("toJSON", {
    virtuals: true,
    versionKey: false,
    transform: function(doc, ret) {   delete ret._id; delete ret.isDeleted;}
});
TaskSchema.virtual('tasks', {
    ref: 'Task',
    localField: '_id',
    foreignField: 'parentTaskId',
    options: {sort: {status: 1, orderPosition: 1}}
});

const TaskModel = mongoose.model('Task', TaskSchema);

module.exports.findByCreatorId = function(creatorId, callback) {
    TaskModel
        .find({ creatorId: creatorId, isDeleted: false, parentTaskId: null} )
        .populate("tasks")
        .sort({status: 1, orderPosition: 1})
        .exec(callback);
};

module.exports.findById = function (id, callback) {
    TaskModel.findOne({_id: id}).populate("tasks").exec(callback)
};

module.exports.create = function(taskObj, callback) {
    const task = new TaskModel({
        name: taskObj.name,
        description: taskObj.description,
        status: taskObj.status || 0,
        orderPosition: taskObj.orderPosition || 0,
        creatorId: taskObj.creatorId,
        parentTaskId: taskObj.parentTaskId || null,
        createdAt: taskObj.createdAt,
        modifiedAt: taskObj.modifiedAt,
        expiredAt: taskObj.expiredAt
    });
    if(taskObj.parentTaskId) {
        TaskModel.findOne({_id: taskObj.parentTaskId}, function (err, parentTask) {
            if(!err && parentTask && parentTask.creatorId === taskObj.creatorId) {
                task.save(callback);
            }
            else {
                callback(null,null);
            }
        });
    } else {
        task.save(callback);
    }
};

module.exports.update = function(id, creatorId, taskObj, callback) {
    TaskModel.findOne({_id: id}, function (err, taskForUpdate) {
        if(err || !taskForUpdate || taskForUpdate.creatorId !== creatorId) {
            callback(null, null);
        } else {
            var oldPosition = taskForUpdate.orderPosition;
            taskForUpdate.name = taskObj.name;
            taskForUpdate.description = taskObj.description;
            taskForUpdate.status = taskObj.status || 0;
            taskForUpdate.orderPosition = taskObj.orderPosition || 0;
            taskForUpdate.expiredAt = taskObj.expiredAt;
            taskForUpdate.modifiedAt = taskObj.modifiedAt;

            taskForUpdate.save(callback);

            if(taskObj.orderPosition > oldPosition) {
                /*update positions */
                TaskModel.updateMany({_id: {$ne: taskObj.id},
                        $and: [{orderPosition: {$lte: taskObj.orderPosition}}, {orderPosition: {$gt:oldPosition}}],
                        parentTaskId: taskObj.parentTaskId || null },
                    {$inc: {orderPosition: -1}}, {}, function () {});
            } else if(taskObj.orderPosition < oldPosition) {
               TaskModel.updateMany({_id: {$ne: taskObj.id},
                       $and: [{orderPosition: {$gte: taskObj.orderPosition}}, {orderPosition: {$lt: oldPosition}}],
                       parentTaskId: taskObj.parentTaskId || null },
                   {$inc: {orderPosition: 1}}, {}, function () {});
            }

        }
    });
};

module.exports.remove = function(id, creatorId, callback) {
    TaskModel.findOne({_id: id}, function (err, taskForRemove) {
        if(err || !taskForRemove || taskForRemove.creatorId !== creatorId) {
            callback(null, null);
        } else {
            taskForRemove.modifiedAt = new Date();
            taskForRemove.isDeleted = true;
            taskForRemove.save(callback);
        }
    });
};
