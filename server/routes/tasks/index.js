const express = require('express');
const router = express.Router();
const passport = require('passport');
const Tasks = require('../../model/task');

router.use(passport.authenticate('jwt', {session: false}));

const tasks = [
    {id: 1, name:'first TasksResource', position: 1, status: 0},
    {id: 2, name:'second TasksResource', position: 2, status: 0}
];
router.get('/', function(req, res) {
    Tasks.findByCreatorId(req.user.id, function(err, tasksList) {
       if(!err && tasksList) {
           res.send(tasksList);
       } else {
           res.status(500).send(err.message);
       }
    });
});
router.get('/:id', function (req, res) {
    const task = tasks[req.params['id']];
    if(task) {
        res.send(task);
    } else {
        res.sendStatus(404);
    }
});

router.post('/', function (req, res) {
   const taskObj = req.body;
   if(taskObj) {
       taskObj.creatorId = req.user.id;
        Tasks.create(taskObj, function (err, task) {
            if(!err && task) {
                res.send(task);
            } else if(!task) {
                res.sendStatus(404);
            } else {
                res.status(500).send(err.message);
            }
        })
   } else {
       res.status(400).send('Missed body');
   }
});

router.put('/:id', function (req, res) {
   const taskObj = req.body;
   if(taskObj) {
        Tasks.update(req.params['id'], req.user.id, taskObj, function (err, task) {
            if(!err && task) {
                res.send(task);
            } else if(!task) {
                res.sendStatus(404);
            } else {
                res.status(500).send((err ? err.message :'') ||'Server error');
            }
        })
   } else {
       res.status(400).send('Missed body');
   }
});

router.delete('/:id', function (req, res) {
    Tasks.remove(req.params['id'], req.user.id, function (err, task) {
        if(!err && task) {
            res.sendStatus(204);
        } else if(!task) {
            res.sendStatus(404);
        } else {
            res.status(500).send((err ? err.message :'') ||'Server error');
        }
    })
});

module.exports = router;
