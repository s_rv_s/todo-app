const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const passport = require('passport');
const mongoose = require('mongoose');
const app = express();


app.use(bodyParser.json());
app.use(express.static("public"));
app.use(cors({
    origin: "http://localhost:4200"
}));
mongoose.connect('mongodb://localhost/todoApp', {
    useUnifiedTopology: true,
    useCreateIndex: true,
    useNewUrlParser: true
});


/*app.use(session({
    secret: "todo-app-secret",
    key: "session_id",
    resave: false,
    saveUninitialized: false
}));*/

app.use(passport.initialize());
// app.use(passport.session());

require('./auth').init(app);

require('./routes')(app);

app.listen(8000, function() {
    console.log('Server started at http://localhost:8000 ! \n (cors enabled for localhost:4200)')
});
