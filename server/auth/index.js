module.exports = {
  init: require('./init'),
  requireAuth: require('./authMiddleware')
};
