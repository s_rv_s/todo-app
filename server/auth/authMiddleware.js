const passport = require('passport');

function authMiddleware () {
    return function (req, res, next) {
        passport.authenticate('jwt', { session: false });
        if (req.isAuthenticated()) {
            return next()
        }
        res.redirect('/fail')
    }
}

module.exports = authMiddleware;
