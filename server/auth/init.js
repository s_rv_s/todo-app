const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const authMiddleware = require('./authMiddleware');
const User = require('../model/user');
/*

passport.serializeUser(function (user, cb) {
    cb(null, user.id)
});

passport.deserializeUser(function (username, cb) {
    User.findByUsername(username, cb)
});
*/

function init(app) {
    passport.use(new JwtStrategy({
       jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
       secretOrKey: 'todo-app-key',
    }, function (jwt_payload, done) {
            User.findById(jwt_payload.data, function(err, user) {
                if (err) {
                    return done(err, false);
                } else if (user) {
                    return done(null, user);
                } else {
                    return done(null, false);
                }
            })
        }
    ));

    passport.authMiddleware = authMiddleware;

    require('./routes')(app);
}
module.exports = init;
