const requireAuth = require('../auth').requireAuth;
const jwt = require('jsonwebtoken');
const User = require('../model/user');

module.exports = function (app) {
    app.post('/login', function (req, res) {
       const credsObj = req.body || {};
       const username = credsObj.username || '';
       const password = credsObj.password || '';
       if(!username || !password){
           res.status(401).send('Please specify username and/or password');
       } else {
           User.findByUsername(username, function (err, user) {
                if(!user || !User.verifyPassword(password, user)){
                    res.status(401).send('Can\'t found user with \'' + username + '\'');
                } else {
                    const token = jwt.sign({data: user.id}, 'todo-app-key');
                    res.status(200).send({token: token});
                }
           });
       }
    });
    app.all('/fail', function(req, res) {
        res.sendStatus(401);
    });
    app.get('/logout', requireAuth(), function(req, res) {
        req.logout();
        res.sendStatus(200);
    });
};
