import {
  IResourceMethodObservable,
  IResourceMethodObservableStrict,
  ResourceAction,
  ResourceParams,
  ResourceRequestMethod
} from '@ngx-resource/core';
import {Injectable} from '@angular/core';
import {ITask} from '../models/task';
import {RestClient} from './restClient';

@Injectable({ providedIn: 'root'})
@ResourceParams({
  pathPrefix: '/tasks'
})
export class TasksResource  extends RestClient {

  @ResourceAction({
    method: ResourceRequestMethod.Get,
    expectJsonArray: true
  })
  get: IResourceMethodObservable<{}, ITask[]>;

  @ResourceAction({
    method: ResourceRequestMethod.Get,
    path: '/{!id}'
  })
  getOne: IResourceMethodObservable<{id: string}, ITask>;

  @ResourceAction({
    method: ResourceRequestMethod.Post,
  })
  create: IResourceMethodObservableStrict<ITask, {}, {}, ITask>;

  @ResourceAction({
    method: ResourceRequestMethod.Put,
    path: '/{!id}'
  })
  update: IResourceMethodObservableStrict<ITask, {}, {id: string}, ITask>;

@ResourceAction({
    method: ResourceRequestMethod.Delete,
    path: '/{!id}'
  })
  remove: IResourceMethodObservable<{id: string}, any>;

}
