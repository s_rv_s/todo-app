import {IResourceAction, Resource, ResourceHandler} from '@ngx-resource/core';
import {AuthService} from '../modules/auth/services/auth.service';
import {Injectable} from '@angular/core';

@Injectable({ providedIn: 'root'})
export class RestClient extends Resource {

  constructor(
      private resourceHandler: ResourceHandler,
      private authService: AuthService) {
    super(resourceHandler);

  }

  $getUrl(methodOptions: IResourceAction): string | Promise<string> {
    const resPath = super.$getPath();
    return 'http://localhost:8000' + resPath;
  }

  $getHeaders(methodOptions?: any): any {
    const headers: any = {};
    headers.Authorization = 'bearer ' + this.authService.currentTokenValue;
    return headers;
  }

}
