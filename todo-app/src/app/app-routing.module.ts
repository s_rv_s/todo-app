import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthGuard} from './guards/auth.guard';

const appRoutes: Routes = [
  { path: '', redirectTo: '/tasks', pathMatch: 'full'},
  { path: 'tasks',
    loadChildren: () => import('./modules/tasks/tasks.module').then(mod => mod.TasksModule),
    canActivate: [AuthGuard]},
  { path: 'login',
    loadChildren: () => import('./modules/auth/auth.module').then(mod => mod.AuthModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
