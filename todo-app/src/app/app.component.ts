import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from './modules/auth/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLogged: boolean;

  constructor(
    private router: Router,
    private authService: AuthService
  ) {
    this.authService.isLogged.subscribe(isLogged => this.isLogged = isLogged);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
