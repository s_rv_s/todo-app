export enum TaskStatusType {
  TODO,
  DONE
}

export interface ITask {
  id: string;
  name: string;
  description: string;
  expiredAt?: any;
  status: number;
  orderPosition: number;
  parentTaskId: string;
  creatorId: string;
  createdAt: string;
  modifiedAt: string;
  tasks?: Task[];
}

export class Task implements ITask {
  id: string;
  name: string;
  description: string;
  expiredAt?: string;
  status: TaskStatusType;
  orderPosition: number;
  parentTaskId: string;
  creatorId: string;
  createdAt: string;
  modifiedAt: string;
  tasks?: Task[];
}
