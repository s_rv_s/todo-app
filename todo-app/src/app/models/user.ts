export interface IUser {
  id: number;
  username: string;
  password?: string;
  firstName: string;
  lastName: string;
}

export class User implements IUser {

  id: number;
  firstName: string;
  lastName: string;
  password?: string;
  username: string;

  public get fullName(): string {
    return ((this.firstName || '') + ' ' + (this.lastName || '')).trim();
  }
}
