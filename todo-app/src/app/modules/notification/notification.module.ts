import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToastrModule} from 'ngx-toastr';
import {AlertService} from './services/alert.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      autoDismiss: true,
      countDuplicates: true,
      resetTimeoutOnDuplicate: true
    }),
  ],
  providers: [
    AlertService
  ]
})
export class NotificationModule { }
