import {Injectable} from '@angular/core';
import {TasksResource} from '../../../resources/tasks.resource';
import {Observable} from 'rxjs';
import {AlertService} from '../../notification/services/alert.service';
import {Task} from '../../../models/task';

@Injectable({providedIn: 'root'})
export class TasksService {

  constructor(
    private tasksResource: TasksResource,
    private alertService: AlertService) { }

  loadTasks(): Observable<Task[]> {
    return this.tasksResource.get((tasks) => {
      this.alertService.instance.success('loaded :)');
      return tasks;
    }, (err) => {
      this.alertService.instance.error('Can\'t load tasks list');
    });
  }

  loadTask(id: string): Observable<Task> {
    return this.tasksResource.getOne({id}, () => {} , (err) => {
      this.alertService.instance.error('Can\'t load task');
    });
  }

  createTask(task: Task): Observable<Task> {
    return this.tasksResource.create(task, (createdTask) => {
      this.alertService.instance.success('Task created :)');
      return createdTask;
    }, (err) => {
      this.alertService.instance.error('Can\'t save task :(');
    });
  }

  updateTask(task: Task): Observable<Task> {
    return this.tasksResource.update(task, {}, {id: task.id}, (updatedTask) => {
      this.alertService.instance.success('Task updated :)');
      return updatedTask;
    }, (err) => {
      this.alertService.instance.error('Can\'t update task :(');
    });
  }

  removeTask(task: Task): Observable<any> {
    return this.tasksResource.remove({id: task.id}, () => {
      this.alertService.instance.success('Task removed :)');
    }, (err) => {
      this.alertService.instance.error('Can\'t update task :(');
    });
  }
}
