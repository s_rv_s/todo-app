import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TasksRoutingModule} from './tasks-routing.module';
import {ResourceModule} from '@ngx-resource/handler-ngx-http';
import {NotificationModule} from '../notification/notification.module';
import {MomentModule} from 'angular2-moment';
import {BsDatepickerModule, CollapseModule, ModalModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {TasksComponent} from './components/tasks/tasks.component';


@NgModule({
  declarations: [
    TasksComponent
  ],
  imports: [
    CommonModule,
    TasksRoutingModule,
    ResourceModule.forChild(),
    NotificationModule,
    MomentModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DragDropModule,
    FormsModule
  ]
})
export class TasksModule { }
