import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {TasksComponent} from './components/tasks/tasks.component';

const tasksRoutes: Routes = [
  { path: '', pathMatch: 'full', component: TasksComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(tasksRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TasksRoutingModule { }
