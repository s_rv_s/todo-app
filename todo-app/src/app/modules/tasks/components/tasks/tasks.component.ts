import {Component, OnDestroy, OnInit} from '@angular/core';
import {TasksService} from '../../services/tasks.service';
import {Task, TaskStatusType} from '../../../../models/task';
import * as moment from 'moment';
import {moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit, OnDestroy {
  TaskStatusType = TaskStatusType;
  tasks: Task[] = [];
  taskSelected: Task = undefined;
  taskSelectedList: Task[] = undefined;

  private taskEdited: Task;
  confirmHelper: {confirm, decline?};

  constructor(
              private tasksService: TasksService,
  ) {  }

  ngOnInit() {
    this.tasksService.loadTasks().subscribe(tasks => {
      this.tasks = tasks;
    });
  }

  ngOnDestroy(): void {
  }

  createTask(list: Task[] = this.tasks) {
    this.taskSelected = undefined;
    this.taskSelectedList = list ;
    const task = new Task();
    task.createdAt = moment().format('YYYY-MM-DD');
    task.modifiedAt = moment().format('YYYY-MM-DD');
    task.status = TaskStatusType.TODO;
    task.orderPosition = list.length;
    this.taskSelected = task;
  }

  saveTask() {
    this.taskSelected.modifiedAt = moment().format('YYYY-MM-DD');
    if (!this.taskSelected.id) {
      this.tasksService.createTask(this.taskSelected).subscribe((task) => {
        this.taskSelectedList.push(task);
        this.taskSelected = undefined;
        this.taskSelectedList = undefined;
      });
    } else {
      this.tasksService.updateTask(this.taskSelected).subscribe((task) => {
        Object.assign(this.taskEdited, task);
        this.taskEdited = undefined;
        this.taskSelected = undefined;
        this.taskSelectedList  = undefined;
      });
    }
  }

  editTask(task) {
    this.taskEdited = task;
    this.taskSelected = Object.assign({}, task);
  }

  setTaskExpiredValue(val) {
    this.taskSelected.expiredAt = val && moment(val).format('YYYY-MM-DD') || undefined;
  }

  markDoneTask(task: Task) {
    const taskForUpdate = Object.assign({}, task);
    taskForUpdate.status = TaskStatusType.DONE;
    this.tasksService.updateTask(taskForUpdate).subscribe((taskUpd) => {
      Object.assign(task, taskUpd);
    });
  }

  onModifyTaskInOrder(event, list: Task[]) {
    this.moveTaskBetweenList(event);
    const task = list[event.currentIndex];
    this.tasksService.updateTask(task).subscribe((taskUpd) => {
      Object.assign(task, taskUpd);
      if (event.currentIndex > event.previousIndex) {
        list.filter(tsk => {
          return tsk.id !== task.id && tsk.orderPosition <= event.currentIndex && tsk.orderPosition > event.previousIndex;
        }).forEach((tsk => {
          tsk.orderPosition -= 1;
        }));
      } else if (event.currentIndex < event.previousIndex) {
        list.filter(tsk => {
          return tsk.id !== task.id && tsk.orderPosition >=  event.currentIndex && tsk.orderPosition < event.previousIndex;
        }).forEach((tsk => {
          tsk.orderPosition += 1;
        }));
      }
    });
  }

  createSubTask(task: Task) {
    this.createTask(task.tasks);
    this.taskSelected.parentTaskId = task.id;
  }

  removeTask(task: Task, tasks: Task[], idx: number) {
    this.confirmHelper = {
      confirm: () => {
        this.tasksService.removeTask(task).subscribe(() => {
          tasks.splice(idx, 1);
          this.confirmHelper = undefined;
        });
      },
      decline: () => {
        this.confirmHelper = undefined;
      }
    };
  }

  taskIsExpiredToday(task: Task) {
    return task.expiredAt && moment(task.expiredAt, 'YYYY-MM-DD').isSame(moment(moment().format('YYYY-MM-DD')));
  }

  taskIsExpiredAlready(task: Task) {
    return task.expiredAt && moment(task.expiredAt).isValid() && moment(task.expiredAt).isBefore(moment(moment().format('YYYY-MM-DD')));
  }


  private moveTaskBetweenList(event) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }
    const task = event.container.data[event.currentIndex];
    task.orderPosition = event.currentIndex;
  }
}
