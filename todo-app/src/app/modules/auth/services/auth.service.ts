import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../../../models/user';

@Injectable({providedIn: 'root'})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  private currentTokenSubject: BehaviorSubject<string>;
  public currentUser: Observable<User>;
  public isLogged: Observable<boolean>;

  constructor(
    private http: HttpClient
  ) {
    this.currentTokenSubject = new BehaviorSubject<string>(localStorage.getItem('currentToken'));
    this.isLogged = this.currentTokenSubject.asObservable().pipe(map(token => !!token));
    this.currentUserSubject = new BehaviorSubject<User>(null);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public get currentTokenValue(): string {
    return this.currentTokenSubject.value;
  }

  login(username, password): Observable<string> {
    return this.http.post<any>('http://localhost:8000/login', {username, password})
      .pipe(map(tokenObj => {
        localStorage.setItem('currentToken', tokenObj.token);
        this.currentTokenSubject.next(tokenObj.token);
        return tokenObj.token;
      }));
  }

  logout() {
    localStorage.removeItem('currentToken');
    this.currentTokenSubject.next(null);
    this.currentUserSubject.next(null);
  }
}
